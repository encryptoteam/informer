module gitlab.com/encryptoteam/rocket-apps/services/informer

go 1.16

// replace gitlab.com/encryptoteam/rocket-apps/services/proto => /Users/dev/projects/gitlab.com/encryptoteam/rocket-apps/services/proto
// replace github.com/sergey-shpilevskiy/go-cert => /Users/dev/projects/github.com/sergey-shpilevskiy/go-cert

require (
	github.com/bykovme/goconfig v0.0.0-20170717154220-caa70d3abfca
	github.com/google/uuid v1.3.0
	github.com/tidwall/gjson v1.14.1
	gitlab.com/encryptoteam/rocket-apps/services/proto v0.7.1
	golang.org/x/net v0.0.0-20220531201128-c960675eff93 // indirect
	google.golang.org/genproto v0.0.0-20220602131408-e326c6e8e9c8 // indirect
	google.golang.org/grpc v1.50.1
)
